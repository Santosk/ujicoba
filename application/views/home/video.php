

<!-- Start campaian video Section -->

<section class="bg-compaian-video">
    <div class="compaian-video-overlay">
        <div class="container">
            <div class="row">
                <div class="compaian-video">
                    <!-- <a href="https://www.youtube.com/embed/imVlGxbHxEo" data-rel="lightcase:myCollection"><img src="<?php echo base_url() ?>assets/tema/assets/images/home02/video-icon.png" alt="video-icon" /></a>
                    <h3>WATCH OUR LATEST CAMPAIGN VIDEO</h3> -->
                    <?php foreach($video as $video) {  ?>
                    <iframe width="100%" height="500"
src="https://www.youtube.com/embed/<?php echo $video->video; ?>?autoplay=1&mute=1">
</iframe>
                        <?php } ?>
                </div>
                <!-- .compaian-video -->
            </div>
            <!-- .row -->
        </div>
        <!-- .container -->
    </div>
    <!-- .compaian-video-overlay -->
</section>

<!-- End campaian video Section -->
